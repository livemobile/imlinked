(function($, window, undefined){
    var home = 'https://imlinked.biz/';
    //var home = 'http://192.168.1.139/imlinked/';
    var MyProfile = {}, // Global, content: id, firstname, lastname. Used to be Global.my
        ConnectionClickedId = {}, //Global, clickedId. Used to beGlobal.connection            
        Connections = [], //Global. Used to be Global.
        ConnectionsPicture = {},
        checkMessageTime = '', //checkMessageTime,

        notificationIdList = [],
        
        pageEventControl = false,
        showOldMessageBtnControl = false,
        feedbackSubmitBtnControl = false,
        connectionsClickedControl = false,
        inviteBtnClickedControl = false,
        invite = false ; // invite friends


    /***
     * bref: when mobile is initial 
     */
    $(document).bind('mobileinit', function () {

        authentication();
        onLinkedInLoad(); 

        /* on screen size less then 480 * 480 situation */
        if (new Imlinked().isSmallScreen()) { // handle, in mobile, user refresh any page that is not home page, will go back to home
           window.onload = function () {
             var url = $.mobile.path.parseUrl(window.location.href);
                if (url.hash !== '') {
                    window.location.href = home ;
                }
           }
        /* large screen size situation */
        }  else {

            showUserProfile();

            /*click user picutre and show user profile*/
            $(document).on('click','.userPicture',function () {
                var url = "/people/id="+$(this).closest('a').attr('id');
                IN.API.Raw(url).result(function (result) {
                   window.open(result.siteStandardProfileRequest.url, '_blank' );// <- This is what makes it open in a new window. 
                });
                return false;
            });
        }

        /***
         * bref: befor main page show
         */
        $(document).on('pagebeforeshow','#main',function () { 
            /* for iphone
            if (
                ("standalone" in window.navigator) &&
                !window.navigator.standalone
                ){
                 //alert('iphone?');
                 $('#forIphoneLogin').show();
            }
            */
            translate();
            buildNotificationButton(); 


            $('.menu-right').find('form').remove(); // this is for search field in the list, 
                                                    //if not remove it, search filed will show out in the notifcation bar

            $('form').find('input').attr('placeholder',$('#placeholderForSearchBar').text()); // set search placeholder in the search filed

            if (new Imlinked().isSmallScreen()) {
                $('#forIphoneLogin').show(); //in mobile, if the linkedin sign in button cannot used. TODO: change to php OAuth?
                ConnectionClickedId = ''; // reset clicked id
                $('div:jqmData(role="header") > h1').css({'margin-right':'0px','margin-left':'5px'}); // in mobile, control header view
                $('#main .header .menu-right').show();
            } else {
                $('#main .header .menu-right').hide();
            }
            
           
        });

       /***
         * bref: befor all page show
         */
        $(document).on('pagebeforeshow','#all',function () {

            $('.menu-right').find('form').remove(); // remove search filed in notification bar
            $('form').find('input').attr('placeholder',$('#placeholderForSearchBar').text()); // set search placheholder
           
            window.onload = function () { // when '#all' page refresh 
                $('#all *').remove();
                window.location.href = home ;
            }

            if (new Imlinked().isSmallScreen()) {
                ConnectionClickedId = ''; //clean clicked id
            }

            $('#all div:jqmData(role="content")').find('ul').listview('refresh');

            $('a:[href="#all"]').click(function () {
                $('#allConnectionsList li').show();
            });
        });

       /***
         * bref: befor feedback page show
         */
        $(document).on('pagebeforeshow','#feedback',function () {
            
            feedbackSubmitBtnClicked();

            window.onload = function () {
                 window.location.href = home ;
            }

            ConnectionClickedId = '';
        }); 

       /***
         * bref: befor messages page show
         */
        $(document).on('pagebeforeshow','#messages',function(){                
            
            showOldMessageBtnClicked();

            if(new Imlinked().isBigScreen()){
                //showUserProfile(); TODO
            }

            window.onload = function(){
                 window.location.href = home ;
            }
             
            $('.menu-right').find('form').remove();
            $('form').find('input').attr('placeholder',$('#placeholderForSearchBar').text());
        });

       /***
         * bref: when messages page show
         */
        $(document).on('pageshow','#messages',function(){ 

            autoScroll();
        }); 
    }); // end of mobileinit


/* =============== */
/* === EVENT  ==== */
/* =============== */

   /***
     * bref: click on company list
     */
    var companyClicked = function() { //company list clicked

        $(document).on('click','a.companies',function(){
                var $self = $(this),
                    $text = $self.text(),
                    headerHeight = $('#main').find('div:jqmData(role="header")').height(); // setting first 'li' margin-top

                $('#all').find('h1').text($text); // set company page title in header bar
                
                getPeopleFromCompany($text);

        });
    };

   /***
     * bref: click on connections list
     */
    var connectionsClicked = function () { // connection list clicked
        if (!connectionsClickedControl) {
            connectionsClickedControl = true;
        } else { return; }

        $(document).on('click','a.Connections',function () {

            var self = this,
                connectionID = $(self).attr('id'),
                connectionName = $(self).text();

            ConnectionClickedId = connectionID;  // set clicked id global variable

            showMessageOnPage(MyProfile.Id, connectionName, connectionID, checkMessageTime );


            if (checkFriendExist(ConnectionClickedId)) { // show invite dialog
                invite = false; 
            } else {
                invite = true;
            }

            //clear notification
            for (var i in notificationIdList) {
                if (ConnectionClickedId === notificationIdList[i]) {
                    notificationIdList.splice(i,1);
                }
            }
        });
    };

   /***
     * bref: keypress in message input field
     */
    var textBoxKeyPress = function() { //message input field key press event
        //alert('here');
        if (!pageEventControl) {
           pageEventControl = true;
        } else { return; }

        $(document).on('keypress','#messageInputField', function(e){
            var key = e.keyCode,
                self = this,
                messageList = $('.comments .comment').length,

                emptyInput = $('#messageInputField').val().replace(/\s|\n/g,'').length === 0 ? true : false,

                data = { // send the message that user inputed to server
                    'text': $(self).val(),
                    'sendFrom': MyProfile.Id,
                    'sendTo': ConnectionClickedId
                }, 

                currentTime,
                currentTimeString;


            if (key === 13 && !e.shiftKey) { // press enter send message while press shit+enter is new line
                e.preventDefault();

                currentTime = new Date(),
                currentTimeString = new Imlinked().getMyTime(currentTime);

                if (!emptyInput) { //user cannot send empty message or space  
                    
                    if ($(self).val().length >= 255) {
                        alert($('#messageTooLong').text());
                    } else {
                        var messageHtml = new Imlinked().messageHtml(ConnectionsPicture[MyProfile.Id], //img
                                                                     '', // message
                                                                     MyProfile.firstName + ' ' + MyProfile.lastName,  //username
                                                                     MyProfile.Id, //userId
                                                                     currentTimeString
                                                                    );
                        $('.comments').append(messageHtml);
                        $('.comments p.secureTxt:last').text($(self).val()); //print the message to seceen and prevent javascript injection
                        $(self).val('');                            
                        
                        sendMessageToServer(data);

                        /*if overlap happening, extend bottom inputfiled*/
                        if (overlaps($('#messageInputField'),$('.comments:last'))) {
                            $('#wrapper').removeClass().addClass('gotMessages');
                        }

                        autoScroll();
                    }

                    if (invite) { //if user not exists in server, show invite dialog
                        inviteFriend();
                        invite = false;
                    }

                } else { $(self).val(''); }
            }
        });
    }

   /***
     * bref: invite button clicked
     */
    var inviteBtnClicked = function () { // invite new connection
        if (!inviteBtnClickedControl) { 
            inviteBtnClickedControl = true;
        } else { return; }

        $(document).on('click','#inviteBtn',function(){
        //$('#inviteBtn').live('click',function(){
            var jsonBody = createInviteJsonBody(), //create json body to use linkedin api
                url = "/people/~/mailbox"; 

            IN.API.Raw(url).method("POST").body(jsonBody).result(function(result){});
        });
    };


   /***
     * bref: show more message button clicked
     */
    var showOldMessageBtnClicked = function() {
         //alert('messagePage show');
        if (!showOldMessageBtnControl) {
            showOldMessageBtnControl = true;
        } else { return; }

        $(document).on('click', '#showOldMessageBtn', function(){
            var dataSendToServer = {
                    'sendFrom': MyProfile.Id,
                    'sendTo': ConnectionClickedId , // send from another id
                    'time': getOldMessageTime
                };        
            //alert('clicked show old button : ' + getOldMessageTime);  
            getOldMessages(dataSendToServer,false);

        });
    };    

   /***
     * bref: feedback submit button clicked
     */
    var feedbackSubmitBtnClicked = function(){
        if (!feedbackSubmitBtnControl) {
            feedbackSubmitBtnControl = true;
        } else { return; }

        $(document).on('click','#feedbackSendBtn',function(){
            var feedback = {};
            if (MyProfile.firstName === undefined || MyProfile.firstName === '' ||  // if user not login and they want to feedback
                MyProfile.lastName === undefined || MyProfile.lastName === '' || 
                MyProfile.Id === undefined || MyProfile.Id === '') {

                feedback = {
                    feedbackContent : $('#feedbackContent').val(),
                    userName : 'Unloggedin User',
                    userId: 'empty'
                };

            } else {
                feedback = {
                    feedbackContent : $('#feedbackContent').val(),
                    userName : MyProfile.firstName+' '+MyProfile.lastName,
                    userId: MyProfile.Id
                };
            }

            if ($('#feedbackContent').val() === '') {
            } else {
                $('#feedbackContent').val('');
                $.ajax({
                    url:'sys/index.php',
                    type:'post',
                    data:feedback,
                    success:function(){
                        alert('Thanks for your feedback!');
                    }
                });
            }
        });
    };

   /***
     * bref: linkedin event. Authanticate using javascript api
     */
    function authentication(){
        IN.init({
            api_key: '3zy2p9iceuwa',
            authorize: true
        });
    }

   /***
     * bref: linkedin event. Authanticate successful
     */
    function onLinkedInLoad() {
        IN.Event.on(IN, "auth", function(){
            $('#signIn').remove();
            $('#forIphoneLogin').hide();
            $('#loadingCompanies').show()
            //$('#loggedin').show();
            if (new Imlinked().isSmallScreen()) {
                $('#loggedin').append('<span class="lang" value="13"><!--You have already signed in imLinked--></span>');
                translate();
                //var curl = $.mobile.path.parseUrl(document.URL); //refresh listview 
                if (window.location.href !== home) {
                    window.location.href= home;
                }                       
            }
            getMyId();
            
        });
    }
/* =================== */
/* ===== FUNCTION ==== */
/* =================== */

   /***
     * bref: linkedin function. share 
     */
    function share(){
        var url = "/people/~/shares",
            jsonBody = {
                "comment":"Is now using imlinked. The Business IM",
                "content":{
                    "title":"imlinked",
                    "description":"imLinked is the instant messaging service for professionals.",
                    "submitted-url":"http:\/\/imlinked.biz",
                    "submitted-image-url":"http:\/\/imlinked.biz\/images\/thum_180_113.jpg"
                },
                "visibility":{
                    "code":"anyone"
                }
            };

        IN.API.Raw(url).method('POST').body(JSON.stringify(jsonBody))
                        .result(function(result){});
    }

   /***
     * bref: get user profile from linkedin
     */
    function getMyId() { // login 
            IN.API.Profile('me').fields('first-name,last-name,id,picture-url').result(function(result) {

            MyProfile.Id = result.values[0].id;
            MyProfile.firstName = result.values[0].firstName;
            MyProfile.lastName = result.values[0].lastName;


            if (result.values[0].hasOwnProperty('pictureUrl')) {
                ConnectionsPicture[MyProfile.Id] = result.values[0].pictureUrl.replace('http','https').replace('://m3','://m3-s');
            } else {
                ConnectionsPicture[MyProfile.Id] = 'images/default.png';
            }

            var userName = MyProfile.firstName +' '+ MyProfile.lastName;
            checkUserExist(MyProfile.Id, userName);                    

        });
    }

    /***
     * bref: get user exist or not exist in our system
     * @param: {String} userid
     * @param: {String} userName
     */
    function checkUserExist(id, userName){
        ajaxErrorHandler();
        $.ajax({
            url:'sys/index.php',
            data:{'userID': id, 'userName': userName},
            dataType: 'json',
            cache:false,
            success: function(data){
                
                if(0 == data.lastActiveTime)//first time on the system
                {share();}

                kickOff(data.lastActiveTime, data.loginTime);
            }
        });
    }

    /***
     * bref: starting get connections stuff
     * @param: {String} user last time send message or last login time
     * @param: {String} user this time login time
     */
    function kickOff(lastActiveTime, loginTime) {

        /* testing spend time for connect with linkedin*/
        var linkedinTimerStart = new Date().getTime();

        var url = "/people/~/connections:(first-name,last-name,picture-url,id,positions:(is-current,company:(name)))";
        
        IN.API.Raw(url).result(function(result){

            /*get result, finish connection with linkedin*/
            var linkedinTimerEnd = new Date().getTime();

            console.log("linkedin start: " +linkedinTimerStart+ " linkedin end: " +linkedinTimerEnd + " during: " + (linkedinTimerEnd-linkedinTimerStart) );

            /* testing spend time for bulding connections stuff*/
            var buildEverythingTimerStart = new Date().getTime();

            getConnectionsProfiles(result.values); //get friends list

            var buildEverythingTimerEnd = new Date().getTime();
            console.log("buildEverything start: " +buildEverythingTimerStart+ " buildEverything end: " +buildEverythingTimerEnd + " during: " + (buildEverythingTimerEnd-buildEverythingTimerStart) );
            
            notification(lastActiveTime); // when user login, check whether there are messages
            
            checkMessageTime = loginTime; // use to check new messages coming in
            getOldMessageTime = loginTime // use to check olde messages when click any connection
            
            setInterval(function(){
                checkNewMessages(checkMessageTime);
            },5000);            

        });
    }


    /***
     * bref: get connections profiles
     * @param: {Object} connections big object, from linkedin
     */

    function getConnectionsProfiles(tmpObj){ // get connection's details
        //alert('getConnectionsProfiles');
        $(tmpObj).each(function(index,value){
            
            var tmp = {};
            if (value.id !== 'private' && value.firstName !== 'private' && value.lastName !== 'private' ) {
                tmp.firstName = value.firstName;
                tmp.lastName = value.lastName;
                tmp.id = value.id;

                if (value.hasOwnProperty('pictureUrl')) {
                     tmp.pictureUrl = value.pictureUrl.replace('http','https').replace('://m3','://m3-s');
                 } else {
                     tmp.pictureUrl = 'images/default.png';
                 }

                //console.log(value);
                tmp.company = {};
                var tmpPositions = value;   //get company

                if (tmpPositions.hasOwnProperty('positions')) {
                    if (tmpPositions.positions["_total"] === 0) {
                        tmp.company['No Current Company'] = 'No Current Company';
                    } else {

                        for (var index in tmpPositions.positions.values) {
                            if (tmpPositions.positions.values[index].isCurrent === true) {
                                tmp.company[tmpPositions.positions.values[index].company.name] = tmpPositions.positions.values[index].company.name;
                            }
                        }    

                    }
                }

                ConnectionsPicture[tmp.id] = tmp.pictureUrl;
                Connections.push(tmp); // build global connections array
            }
    
        });                

        getCompanies();
        createAllFriends();
    }

    /**
     * bref: controll list view of notification 
     */
    function notificationViewControll () {

        var curl = $.mobile.path.parseUrl(document.URL); //refresh listview 
            if (new Imlinked().isSmallScreen()) {
                switch (curl.hash) {
                case '': 
                    $('#main .newMessagesList').listview('refresh');
                    $('#main .newMessagesList').find('span.ui-icon-arrow-r').remove();
                    break;
                case '#all':
                    $('#all .newMessagesList').listview('refresh');
                    $('#all .newMessagesList').find('span.ui-icon-arrow-r').remove();
                    break;
                case '#imLinkedWelcome':
                    $('#imLinkedWelcome .newMessagesList').listview('refresh');
                    $('#imLinkedWelcome .newMessagesList').find('span.ui-icon-arrow-r').remove();
                    break;
                case '#messages':
                    $('#messages .newMessagesList').listview('refresh');
                    $('#messages .newMessagesList').find('span.ui-icon-arrow-r').remove();
                    break;
                default:
                    //alert(document.URL);
               }

            } else { 
                switch (curl.hash) {
                case '':
                    $('#imLinkedWelcome .newMessagesList').listview('refresh');
                    $('.newMessagesList').find('span.ui-icon-arrow-r').remove();
                    break;
                case '#messages':
                   // $('#imLinkedWelcome .newMessagesList').listview('refresh');
                    $('#messages .newMessagesList').listview('refresh');
                    $('.newMessagesList').find('span.ui-icon-arrow-r').remove();
                    break;
                default:
                    //alert(document.URL);
                }
            }
    }
    /**
     * bref: login notificiation, when user login ,check whether there are messages since they logout last time
     */
    function notification (lastActiveTime) {
        ajaxErrorHandler();
        $.ajax({
            url:'sys/index.php',
            data:{'lastActiveTime':lastActiveTime, 'id': MyProfile.Id},
            dataType: 'json',
            cache : false,
            success: function(data) {

                if ($.isEmptyObject(data)) { // there is no any messages after user last time leave 

                } else {
                   
                    setNotifications(data);
                    notificationViewControll();

                    /* --- there are notification messages then change notification button theme and enable it ---*/
                    $('.menu-right span:jqmData(role="button")').attr('data-theme','c').removeClass('ui-btn-up-a ui-btn-hover-a').addClass('ui-btn-up-c'); 
                    $('.menu-right span:jqmData(role="button")').removeClass('ui-disabled');

                }
            }
        });
    }

    /**
     * bref: when get notification when user login, show notifications
     * @param {Object} data from notification ajax success response
     */
    function setNotifications (data) { 
        var notifyList = {};

        for (var i in data) {
            notifyList[data[i].send_from_id] = data[i].userName;
        }

        for (var id in notifyList) {
            $('<li class="'+id+'"><a data-panel="main" href="#messages" style="padding-right:0px"><span class="userId" style="display:none">'+id+'</span>'+'<img class="ui-li-icon" style="top: 0px; left: 0px; right: 17px;"  src="'+ConnectionsPicture[id]+'">'+'<span class="name">'+notifyList[id]+'</span></a></li>').appendTo('.newMessagesList');
        }
    }


              
    /***
     * bref: get all companies from connections array, and sort by name, and unique, in order to create company list
     */
    function getCompanies () { //fetch out companies

        var companyArray = [];

        for (var i in Connections) {
            for(var n in Connections[i].company){
                companyArray.push(Connections[i].company[n]);
            }
        }                        
        companyArray.sort(); 
        createCompaniesList(uniqueArray(companyArray));
    }

    /**
     * bref: to get unique array 
     */
    function uniqueArray (array) { // company should be uniq, without same companies
       var u = {}, a = [];
       for (var i = 0, l = array.length; i < l; ++i) {
          if (u.hasOwnProperty(array[i])) {
             continue;
          }
          a.push(array[i]);
          u[array[i]] = 1;
       }
       return a;
    }

    /**
     * bref: create the company list and render it to main page
     * {Object} sorted and unique company array
     */
    function createCompaniesList (companies) { // create list
        var companyHtml = '';

        var noCurrentCompanyCount = false;
        for (var i = 0, len = companies.length; i < len; i += 1) {
            if (companies[i] !== 'No Current Company') {
                companyHtml += '<li data-theme="c" class="ui-btn ui-btn-icon-right ui-li-has-arrow ui-li ui-btn-up-c"><div class="ui-btn-inner ui-li" aria-hidden="true"><div class="ui-btn-text"><a href="#all" class="ui-link-inherit companies"><span>'+companies[i]+'</span></a></div><span class="ui-icon ui-icon-arrow-r ui-icon-shadow"></span></div></li>'; 
                
            } else {
                noCurrentCompanyCount = true;
            }
        }

        if (noCurrentCompanyCount) {
            companyHtml += '<li data-theme="c" class="ui-btn ui-btn-icon-right ui-li-has-arrow ui-li ui-btn-up-c"><div style="background-color:#bbf" class="ui-btn-inner ui-li" aria-hidden="true"><div class="ui-btn-text"><a href="#all" class="ui-link-inherit companies"><span>No Current Company</span></a></div><span class="ui-icon ui-icon-arrow-r ui-icon-shadow"></span></div></li>'
        }

        $('#companiesList').html(companyHtml);

        /* on big screen need to margin top and bottom in order to display right */
        if (new Imlinked().isBigScreen()) { //on desktop need to margin top to show company list right
            $('#main div:jqmData(role="content")').css('margin-top', $('#main div:jqmData(role="header")').height());
            $('#main div:jqmData(role="content")').css('margin-bottom', $('#main div:jqmData(role="footer")').height());
        }

        companyClicked(); 
        $('#loadingCompanies').hide();
    }



    /**
     * bref: '#all' page layout controll and enable click event on connections
     */

    function createAllFriendsPageControll () {

        $('#all').live('pagebeforeshow', function(){
            if (new Imlinked().isSmallScreen()) { //in mobile show the notification button in header bar
                $('div:jqmData(role="header") > h1').css({'margin-right':'0px','margin-left':'5px'});
                $('#all .header .menu-right').show();
            } else {
                $('#all .header .menu-right').hide();
            }
        });
        $('#all').live('pageshow',function(){
            if (new Imlinked().isBigScreen()) {
                $('#all div:jqmData(role="content")').css('margin-top', $('#all div:jqmData(role="header")').height());
                $('#all div:jqmData(role="content")').css('margin-bottom', $('#all div:jqmData(role="footer")').height());
            }
        });

        connectionsClicked();
    }

    /**
     * bref: create all connections list
     */
    function createAllFriends () { // list all connections
        var connectionsHtml = '<ul data-filter="true" data-role="listview" id="allConnectionsList">',
            len = Connections.length,
            userIds = [],
            userStatus = {};

        for (var i in Connections) {
            userIds.push(Connections[i].id);
        }

        userStatus = checkConnectionStatus(userIds);

        for (var i = 0; i < len ; i++) {
           connectionsHtml = buildConnectionsList(Connections[i], connectionsHtml, userStatus[Connections[i].id]);
        }

        connectionsHtml += '</ul>';
        
        $('#allFriendContent').html(connectionsHtml);
        
        $('#main a.ui-disabled').removeClass('ui-disabled');
        // check user status ever 5 mins
        setInterval(function(){
            userStatus = checkConnectionStatus(userIds);
            for (var i in userStatus) {

                if (!userStatus.hasOwnProperty(i)) continue;

                if (userStatus[i] === 'online') {
                    $('#'+i).removeClass('NotOnline').addClass('Online').css({'border-width':'5px','border-color':'blue'});
                } else {
                    $('#'+i).removeClass('Online').addClass('NotOnline').css('border-width','0px');
                }
            }
        },300000);

        createAllFriendsPageControll();
    }

    function checkConnectionStatus(connectionId) {
        var userStatus = {}; // need to be object
        ajaxErrorHandler();
        $.ajax({
            url:'sys/index.php',
            type:'post',
            data: {'connectionId':connectionId},
            dataType: 'json',
            async: false,
            cache : false,
            success: function(data){
                for (var i in data) {
                    userStatus[data[i].id] = data[i].status;
                }
            }
        });
        return userStatus;
    }

    function buildConnectionsList(connections, container, stat) {
        var state = stat === 'notOnline' ? "NotOnline" : "Online";
        
        container += '<li><a data-panel="main" class="Connections '+state+'" id="'+connections.id+'" href="#messages" >'+'<img class="ui-li-thumb userPicture" src="'+connections.pictureUrl+'">'+connections.firstName+' '+connections.lastName+'</a></li><a class="profileLink" style="display:none" href="#" target="_blank"></a>';

        return container;
    }

    function checkNewMessages(latestMessageTime) { //check new messages comming in
        ajaxErrorHandler();
        $.ajax({
            url: 'sys/index.php',
            //type: 'post',
            dataType: 'json',
            cache: false,
            data: {'new'/*newMessageSendToMe*/: MyProfile.Id, 'latestMessageTime':latestMessageTime}, // time should be updated if new messages came
            success: function(data) {

                if ($.isEmptyObject(data)) {
                        //console.log('---------- there is no new message now -------------');
                } else {

                    var latestMessage = retrieveNewData(data); //last message from another         

                    if (!$.isEmptyObject(latestMessage)) {
                        checkMessageTime = latestMessage.time;
                    }
                }
            }      
        });
    }

    newMessages = {};

    function retrieveNewData(data) { //get new messages 
        var messageHtml = '';
        for (var i = 0, len=data.length; i<len; i++) {
            if (data[i].message !== null && data[i].message !== undefined &&
                data[i].userName && // cannot be falsy value
                data[i].send_from_id && // cannot be falsy value
                data[i].send_to_id  // cannot be falsy value
               ) {
                // store new messages
                newMessages[data[i].send_from_id] = {'time':data[i].time,'message':data[i].message};

                var timeString = new Imlinked().timeConvert(data[i].time);

                if(data[i].send_from_id === ConnectionClickedId){  // chatting with
                    //when click show profile on desktop version
                    messageHtml += new Imlinked().messageHtml(ConnectionsPicture[ConnectionClickedId], //img
                                                              newMessages[ConnectionClickedId].message, // message
                                                              data[i].userName,  //username
                                                              ConnectionClickedId,
                                                              timeString
                                                             );
                } else { // not chatting with, set notification

                    $('.menu-right span:jqmData(role="button")').attr('data-theme','c').removeClass('ui-btn-up-a ui-btn-hover-a').addClass('ui-btn-up-c'); // change notification button color 
                    $('.menu-right span:jqmData(role="button")').removeClass('ui-disabled');
                    if(notificationIdList.indexOf(data[i].send_from_id) === -1){ //notification if this notification from a new guy
                        notificationIdList.push(data[i].send_from_id);
                        $('<li class="'+data[i].send_from_id+'"><a href="#messages" style="padding-right:0px"><span class="userId" style="display:none">'+data[i].send_from_id+'</span>'+'<img class="ui-li-icon" style="top: 0px; left: 0px; right: 17px;" src="'+ConnectionsPicture[data[i].send_from_id]+'">'+'<span class="name">'+data[i].userName+'</span></a></li>').appendTo('.newMessagesList');
                        notificationViewControll();
                    } else {}

                }
            }

        }               

        $('.comments').append(messageHtml);
        autoScroll();

        return data[data.length - 1];
    }


    function getPeopleFromCompany(company) { // list connections according to the company

        for (var i in Connections) {
                if (Connections[i].company[company]) {
                    $('#'+Connections[i].id).closest('li').show();
                } else {
                    $('#'+Connections[i].id).closest('li').hide();
                }

        }
    }

    function createMessageBox() {
        $('.comments').html('');
    }

    function showUserProfile () {

        $(document).on('click','.profileLink',function () {
            var self = this;
                url = "/people/id=" + $(this).attr('rel') + ":(siteStandardProfileRequest)";
            IN.API.Raw(url).result(function (result) {
                $(self).attr('href',result.siteStandardProfileRequest.url)
               //window.open(result.siteStandardProfileRequest.url, '_blank' );// <- This is what makes it open in a new window. 
            });
        });

    }

    function sendMessageToServer(message) {
        ajaxErrorHandler();
        $.ajax({
            url: 'sys/index.php',
            type: 'post',
            data: message,
            dataType: 'json',
            success: function (data){
                checkMessageTime = data.time;
            }
        });
    }

    /**
     * bref: get old messages ajax.
     */       
    function getOldMessages(dataSendToServer,_scrollable) {

        var scrollable = _scrollable === undefined ? true : _scrollable;
        ajaxErrorHandler();
        $.ajax({
            url: 'sys/index.php',
            //type: 'post',
            dataType: 'json',
            data: dataSendToServer,
            cache: false,
            success: function(data) {
                getOldMessagesSuccess(data,dataSendToServer.sendTo, scrollable);            
            }  
        });
    }

    /**
     * bref: get old messages ajax success
     * @param {Object} ajax success response data
     */ 
     function getOldMessagesSuccess(data, sendToId, scrollable) {

        var messageList = $('.comments .comment').length;   

        if ($.isEmptyObject(data)) { // no old messages 

            $('#showOldMessageBtn').addClass('ui-disabled');
            
            if (new Imlinked().isBigScreen()) { // window size control for desktop
                if (messageList === 0) {
                    $('#wrapper').removeClass().addClass('emptyMessages'); 
                } else {
                    $('#wrapper').removeClass();
                }
                
            } else { 
                if (messageList === 0) {
                    $('#messageInputField').css('margin-top','40px');                                   
                }
            }
            
        }  else {

            $('#wrapper').removeClass().addClass('gotMessages');
    
            
            $('#showOldMessageBtn').removeClass('ui-disabled');

            var latestMessage = retrieveData(data, sendToId, scrollable);                     
            
            if (!$.isEmptyObject(latestMessage)) {
                getOldMessageTime = latestMessage.time - 1;
            } else {
                getOldMessageTime = 0;
            } 
        }
    }

    function retrieveData (data, connection, _scrollable) {
        var messageHtml = '',
            scrollable = _scrollable === undefined ? true : _scrollable,
            tmpResult = [];

        for(var i=0,len=data.length; i<len; i += 1){
            if (data[i].message !== null && data[i].message !== undefined &&
                data[i].userName && // cannot be falsy value
                data[i].send_from_id && // cannot be falsy value
                data[i].send_to_id  // cannot be falsy value
               ) {

                if (data[i].send_from_id === connection && data[i].send_to_id === MyProfile.Id) {
                    //receive from connection
                    var timeString = new Imlinked().timeConvert(data[i].time);

                   messageHtml += new Imlinked().messageHtml(ConnectionsPicture[connection], //img
                                                             data[i].message, // message
                                                             data[i].userName,  //username
                                                             connection,
                                                             timeString
                                                            ); 
   
                } else if (data[i].send_from_id === MyProfile.Id && data[i].send_to_id === ConnectionClickedId) {
                    // messages 'I sent' ;

                    var timeString = new Imlinked().timeConvert(data[i].time);

                    messageHtml += new Imlinked().messageHtml(ConnectionsPicture[MyProfile.Id], //img
                                                                 data[i].message, // message
                                                                 data[i].userName,  //username
                                                                 MyProfile.Id,
                                                                 timeString
                                                                );
                } else {
                    console.log('retrieveData id has problem');
                    return '{}';
                }

            } else {
                console.log(data[i]);
                alert('Database error! Please feedback, we will fix that soon!');
                console.log('retrieveData data has problem');
            }
        }

        $('.comments').prepend(messageHtml);

        if (scrollable) autoScroll();
        return data[0];    
    }

    /**
     * bref: show messages on messages page
     * @param: {String} user's id
     * @param: {String} clicked connection's name
     * @param: {String} clicked connection's id
     * @param: {Number} unix time, checkMessageTime
     *
     */
    function showMessageOnPage (myID,connectionName,connectionID,checkMessageTime) {
        
        var dataSendToServer = { // use to check old messages
            'sendFrom': myID,
            'sendTo': connectionID, // send from another id
            'time': checkMessageTime
        };

        getOldMessages(dataSendToServer);

        $('#connectionName').html(connectionName);     // set header bar

        createMessageBox();
        textBoxKeyPress(); 

    }

    function inviteFriend () {
        if (new Imlinked().isSmallScreen()) {
            $('#inviteDialog').click();
        } else {
            $('<div id="invite">').simpledialog2({
                mode: 'blank',
                blankContent:
                '<p><span class="lang" value="16"><span></p>'+
                '<label><span class="lang" value="14"></span> </label>'+
                '<input type="text" id="inviteMessage" style="background-color:white; width: 88%"/>'+
                '<a id="inviteBtn" data-role="button" href="#" rel="close"><span class="lang" value="17"></span></a>'+
                '<a rel="close" data-role="button" href="#"><span class="lang" value="18"></span></a>'                        
            });
        }
        translate();
        inviteBtnClicked();
        $(".ui-simpledialog-container").live('click',function(event){ return false; });
        $(document).one("click", function() { $(".ui-simpledialog-container").fadeOut(); });
    }

    function createInviteJsonBody() {
        var inviteMessage = $('#inviteMessage').val();
        
        if (inviteMessage === '' || inviteMessage === 'undefined' || inviteMessage === null) {
            inviteMessage = 'I would like to connect with you on imLinked.';
        }
        
        var jsonBody = {
                "recipients": {
                    "values": [
                    {
                      "person": {
                        "_path": "/people/"+ConnectionClickedId+"",
                       }
                    }]
                },
                "subject": "One of your associates left you a new message on imLinked.",
                "body": inviteMessage + '\n\nTo get imLinked, please go to http://imlinked.biz'
            };
            
            return JSON.stringify(jsonBody);
         }

    function checkFriendExist(friendId){
        var friendExist ;
        ajaxErrorHandler();
        $.ajax({
            url: 'sys/index.php',
            data: { 'friendId':friendId }, 
            async:false,
            dataType:'json',
            cache: false,
            success: function(data){
                friendExist = data.exist;
            }
        });
        return friendExist;
    }

    function translate(){  // set language and SET BUTTON ICON here    
        var browser_lang = window.navigator.language.substring(0,2);
        var url =  home +'lang/'+ browser_lang + '.json';
        var language = loadLang(url);
        $('.lang').each(function(){
            var self = $(this);
            $.each(language,function(i,e){
                //alert(i);
                if(i===self.attr('value')){
                    self.html(e);
                    //alert(self.html(e));
                }
            });
        });
        setPlaceholder();
    }

    function loadLang(url){
        var lang = null; // language

        $.ajax({
            url:url,
            dataType:'json',
            async:false,
            cache : true,
            error: function(data){ //if cannot get the lang file, redirect to en.json by a .htaccess in the lang folder
                lang = $.parseJSON(data.responseText);
            },
            success: function(data){
                lang = data;
            }
        });
        return lang;
    }

    function setPlaceholder () {
        $('textarea').each(function(){
            var placeholder_message = $(this).next().text();
            $(this).attr('placeholder',placeholder_message);
        }); 
    }


    // new message list
    function buildNotificationButton () {
        
        $(document).on('click', 'ul:jqmData(role="menu")', function(e){
            $('.header').each(function(){
                $(this).trigger('hideOpenMenus');
            });
            $(this).find('li > ul').show();
               e.stopPropagation();
        });

        $("ul:jqmData(role='menu') li > ul li").live('click', function(e) {
             
            var connectionID = $(this).find('.userId').text(),
                connectionName = $(this).find('.name').text();

            $('.header').each(function(){
                $(this).trigger('hideOpenMenus');
            });
            e.stopPropagation();
                
            ConnectionClickedId = connectionID;

            showMessageOnPage(MyProfile.Id,connectionName,connectionID,checkMessageTime);
                
            if (checkFriendExist(connectionID)) {
                invite = false; 
            } else {
                invite = true ;
            }
                
            $('.'+connectionID).remove(); //delete clicked list from notification list
                
            for(var i in notificationIdList){
                if(ConnectionClickedId === notificationIdList[i]){
                    notificationIdList.splice(i,1);
                }
            }

            if (notificationIdList.length === 0) {
                $('.menu-right span:jqmData(role="button")').addClass('ui-disabled');
                $('.menu-right span:jqmData(role="button")').attr('data-theme','a').removeClass('ui-btn-up-c ui-btn-hover-c').addClass('ui-btn-up-a');
            }
        });

        $('.header').each(function(){
            $(this).live('hideOpenMenus', function(){
                $('ul:jqmData(role="menu")').find('li > ul').hide();
            });
        });
        

        $('.header').each( function(){
            $(this).live('click',function(e){
                   $(this).trigger('hideOpenMenus');
             });
        });
    }

    function ajaxErrorHandler(){
            $.ajaxSetup({
                error:function(x,e){
                    if(x.status==0){
                        //alert('You are offline!!\n Please Check Your Network.');
                    }else if(x.status==404){
                        alert('Requested URL not found.');
                    }else if(x.status==500){
                        alert('Internel Server Error.');
                    }else if(e=='parsererror'){
                        alert('Error.\nCannot Access Server'); //parse json error
                    }else if(e=='timeout'){
                        alert('Request Time out.');
                    }else {
                        alert('Unknow Error.\n'+x.responseText);
                    }
                }
            });
    }

function autoScroll(){
        var scrollOffset = $('.comments').outerHeight(); // auto scroll
        if(scrollOffset > 0){
            if(new Imlinked().isSmallScreen() ){
                var bottom = parseInt(scrollOffset) + parseInt($('.ui-footer').height());
                $('html').scrollTo(bottom.toString(), 500);
            } else {
                $('#messages .ui-content').animate({scrollTop:scrollOffset},500);
            }
        }
    }

var overlaps = (function () {
    function getPositions( elem ) {
        var pos, width, height, boder;
        pos = $( elem ).position();
        width = $( elem ).width();
        height = $( elem ).outerHeight();
        return [ [ pos.left, pos.left + width ], [ pos.top, pos.top + height ] ];
    }

    function comparePositions( p1, p2 ) {
        var r1, r2;
        r1 = p1[0] < p2[0] ? p1 : p2;
        r2 = p1[0] < p2[0] ? p2 : p1;
        return r1[1] > r2[0] || r1[0] === r2[0];
    }

    return function ( a, b ) {
        var pos1 = getPositions( a ),
            pos2 = getPositions( b );
        return comparePositions( pos1[0], pos2[0] ) && comparePositions( pos1[1], pos2[1] );
    };

})();

}(jQuery,window));
