<?php

if(empty($_POST) AND empty($_GET)){ exit; }

function my_autoload ($pClassName) 
{
	require_once(str_replace('\\','/',ROOT_PATH . '/' . $pClassName	 . '.php'));// str_replace is a work around for linux systems
}

$result = '{}';//an empty json object should be return is anthing goes basd

ob_start();
try
{

set_error_handler(create_function('$a, $b, $c, $d', 'throw new ErrorException($b, 0, $a, $c, $d);'), E_ALL);

define('ROOT_PATH',dirname(__FILE__));
spl_autoload_register('my_autoload');

	$handler = new \Handler(empty($_GET)?$_POST:$_GET);
	$result = $handler->process(); 
	
	$output = ob_get_contents();
	
	if(0 < strlen($output))//check of unwanted output
	{	throw new Exception('webService is outputting:"'.$output.'" in the middle of processing'); }
}
catch (Exception $_exception) {
	$db = new \Database();
	$db->logError($_exception->getMessage(), $_exception->getTrace());
}

ob_end_clean();//wipe & allow output again

echo ('[]' === $result)?'{}':$result;
?>
