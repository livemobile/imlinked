<?php
$consumer_key = '3zy2p9iceuwa';
$cookie_name = "linkedin_oauth_${consumer_key}";
$credentials_json = $_COOKIE[$cookie_name]; // where PHP stories cookies
$credentials = json_decode($credentials_json);

// validate signature
$consumer_secret = 'QkpmcWyTX5MeMKAZ';
if ($credentials->signature_version == 1) {
    if ($credentials->signature_order && is_array($credentials->signature_order)) {
        $base_string = '';
        // build base string from values ordered by signature_order
        foreach ($credentials->signature_order as $key) {
            if (isset($credentials->$key)) {
                $base_string .= $credentials->$key;
            } else {
                print "missing signature parameter: $key";
            }
        }
        // hex encode an HMAC-SHA1 string
        $signature =  base64_encode(hash_hmac('sha1', $base_string, $consumer_secret, true));
        // check if our signature matches the cookie's
        if ($signature == $credentials->signature) {
            print "signature validation succeeded";
        } else {
            print "signature validation failed \n"."signature: ".$signature."\ncredentials->signature: ".$credentials->signature;    
        }
    } else {
        print "signature order missing";
    }
} else {
    print "unknown cookie version";
}
// configuration settings

$access_token_url = 'https://api.linkedin.com/uas/oauth/accessToken';

// init the client
$oauth = new OAuth($consumer_key, $consumer_secret);
$access_token = $credentials->access_token;

// swap 2.0 token for 1.0a token and secret
$oauth->fetch($access_token_url, array('xoauth_oauth2_access_token' => $access_token), OAUTH_HTTP_METHOD_POST);
// parse the query string received in the response
parse_str($oauth->getLastResponse(), $response);

// Debug information
print "OAuth 1.O Access Token = " . $response['oauth_token'] . "\n";
print "OAuth 1.O Access Token Secret = " . $response['oauth_token_secret'] . "\n";

$oauth->setToken($response['oauth_token'],$response['oauth_token_secret']);





/* old test 
define("CONSUMER_KEY","3zy2p9iceuwa");
define("CONSUMER_SECRET", "QkpmcWyTX5MeMKAZ");

$oauth = new OAuth(CONSUMER_KEY, CONSUMER_SECRET);

$request_token_response = $oauth->getRequestToken('https://api.linkedin.com/uas/oauth/requestToken');
 
if($request_token_response === FALSE) {
        throw new Exception("Failed fetching request token, response was: " . $oauth->getLastResponse());
} else {
        $request_token = $request_token_response;
}

print "Request Token:\n";
printf("    - oauth_token        = %s\n", $request_token['oauth_token']);
printf("    - oauth_token_secret = %s\n", $request_token['oauth_token_secret']);
print "\n";
*/
?>