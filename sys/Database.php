<?php

class Database {
	private $connect;
	
	private $loginTable;
	private $logTable;
	private $messageTable;

	public function __construct() 
	{
	
		$this->loginTable = 'user_login';
		$this->logTable = 'log';
		$this->messageTable = 'user_messages';
		$this->feedbackTable = 'user_feedback';
		
		if(0 === strcasecmp(gethostname(),'punk'))
		{	$sqlServer = 'mysql';	}
		else
		{	$sqlServer = 'dev';	}	
		
		$this->connect = new \PDO('mysql:host='.$sqlServer.';dbname=linkedin','imlinkedAdminDB','880527');
	}

	/**
	 * @brief log error into database
	 *
	 * @desc
	 *
	 */
	public function logError($_message, $_trace)
	{
		if(empty($_message) || empty($_trace))
		{
			throw new InvalidArgumentException("empt message or trace");
		}

		$trace = '\n';
		if(is_array($_trace))
		{
			foreach($_trace as $k=>$v)
			{
				if($v['function'] == 'include' 
				|| $v['function'] == 'include_once' 
				|| $v['function'] == 'require_once' 
				|| $v['function'] == 'require')
				{
					$args = ''; 
					if(isset($v['args']) && is_array($v['args']))
					{
						$size = count($v['args']);
						foreach ($v['args'] as $key => $arg)
						{
							$args .= $v['args'][$key];
							if($key < $size)
							{$args .= ', ';}
						}
					}
					
					$trace .= '#'.$k.' '.$v['function'].'('.$args.') called at ['.$v['file'].':'.$v['line'].']';
				}
				else
				{
					$function = (array_key_exists('function',$v)) ? $v['function'].'() ' : 'function_name';
					$file = (array_key_exists('file',$v)) ? $v['file'] : 'file_name';
					$line = (array_key_exists('line',$v)) ? $v['line'] : 'line';
					$trace .= "#$k $function called at $file:$line\n";
				}
			}
		}
		else 
		{
			$trace = $_trace;
		}

		$exec = $this->connect->prepare('INSERT INTO '.$this->logTable.' (`time` ,`message` ,`trace`)
																	VALUES (:time, :message, :trace);');
	
		$exec->bindValue(':time', time());
		$exec->bindValue(':message', $_message);
		$exec->bindValue(':trace', $trace);

		$exec->execute();
	}

	/**
	 * @brief user login
	 *
	 * @desc
	 *	 	when user login, set user status to be online, set login time, if user first login, lastActive to be 0,
	 *		otherwise lastActivate time is last time user did thing's time
	 *
	 * @param {String} userID from linkedin
	 * @param {String} userName
	 * @param {UnixTime} time
	 *
	 */	
	public function checkUser($_userId,$_userName,$_time)
	{
		if(empty($_userId) || empty($_userName) || empty($_time))
		{
			throw new InvalidArgumentException("Invaild userId: " . var_export($_userId,TRUE) . 
											   " or userName: " .var_export($_userName, TRUE) . 
											   " or time: " . var_export($_time, TRUE));
		}
	
		$exec = $this->connect->prepare('SELECT `time` FROM '.$this->loginTable.' WHERE `id` LIKE :userId');
		
		$exec->bindValue(':userId', $_userId);
		
		$reply = array();
		
		try{

			$exec->execute();
			$result = $exec->fetch(\PDO::FETCH_ASSOC);
			$result = $this->checkPDOerror($result, $exec);//throw RuntimeException
			
				$reply['status']='online';
				$reply['loginTime']=$_time;
							
				if(empty($result)){ //there is no that user
					$exec = $this->connect->prepare('INSERT INTO `'.$this->loginTable.'` (`id`, `time`, `firstTime`, `userName`) VALUES (:userId, :time, :time, :userName)');
					$reply['lastActiveTime'] = 0;

				} else {
					$reply['lastActiveTime'] = intval($result['time']);
					$exec = $this->connect->prepare('UPDATE `'.$this->loginTable.'` SET `time`=:time, `userName`=:userName WHERE `id`=:userId');
				}
				
				$exec->bindValue(':userId', $_userId);
				$exec->bindValue(':time', $_time);
				$exec->bindValue(':userName', $_userName);
				$exec->execute();
				
		} catch (Exception $_exception) {
			$this->logError($_exception->getMessage(), $_exception->getTraceAsString());
		}

		return json_encode($reply);
	}

	/**
	 * @brief check friend exist for send invite message 
	 *
	 * @desc
	 *      if friend is not exist, when send message to friend, show invite dailog
	 *
	 * @param {String} userId
	 */
	public function checkFriendExist($_userId)
	{
		if(empty($_userId))
		{
			throw new InvalidArgumentException("Invaild userId: " . var_export($_userId,TRUE));
		}
		
		$exec = $this->connect->prepare('SELECT `id` FROM '.$this->loginTable.' WHERE `id` LIKE :userId');
		$exec->bindValue(':userId', $_userId);
		$reply = array();
		
		try{

			$exec->execute();
			$result = $exec->fetchAll(\PDO::FETCH_ASSOC);
			$result = $this->checkPDOerror($result, $exec);//throw RuntimeException
			
			$reply['exist'] = !empty($result);
			
		} catch (Exception $_exception) {
			$this->logError($_exception->getMessage(), $_exception->getTraceAsString());
		}
		return json_encode($reply);
	}
	/* Deprecated
	public function getNotificationMessages($_myId,$_lastTime) { // VS checkNewMessages ????

		$exec = $this->connect->prepare('SELECT * FROM '.$this->messageTable.' 
																	WHERE `send_to_id` LIKE :myId 
																	AND `time` > :lastTime 
																	ORDER BY `send_from_id`');
		
		$exec->bindValue(':myId', $_myId);
		$exec->bindValue(':lastTime', $_lastTime);
		
		$reply = array();
		
		try{

			$exec->execute();
			$result = $exec->fetchAll(\PDO::FETCH_ASSOC);
			$result = $this->checkPDOerror($result, $exec);//throw RuntimeException
			
			$reply = json_encode($result);
			
		} catch (Exception $_exception) {
			$this->logError($_exception->getMessage(), $_exception->getTraceAsString());
		}
		
		return $reply;
	}*/

	/**
	 * @brief check users status
	 *
	 * @desc
	 *      check all friends is online or not online
	 *
	 * @param {Array} userIDs
	 */
	public function checkUserStatus(array $_userIds) 
	{
		if(empty($_userIds))
		{
			throw new InvalidArgumentException("Invaild userIds: " . var_export($_userIds,TRUE));
		}
	
		$quary = 'SELECT `id` FROM  '.$this->loginTable.' WHERE `time` > :time AND `id` IN (';
		$count = 0;
		$size = count($_userIds);
		while($count<$size){
			$quary .= ':u'.($count++).',';
		}
		$quary[strlen($quary)-1] = ')';//replace the last ','  with a ')'
		
		$exec = $this->connect->prepare($quary);
		
		$count = 0;
		foreach($_userIds as $id){
			$exec->bindValue((':u'.$count++), $id);
		}

		$exec->bindValue(':time', time() - 300);
		$reply = array();
		
		try{
			$exec->execute();
			$result = $exec->fetch(\PDO::FETCH_ASSOC);			
			$result = $this->checkPDOerror($result, $exec);//throw RuntimeException
			$result = array_flip($result);
			
			foreach($_userIds as $id){
				$reply[] = array('id' => $id,'status' => (array_key_exists($id,$result))?'online':'notOnline');
			}
		
		} catch (Exception $_exception) {
			$this->logError($_exception->getMessage(), $_exception->getTraceAsString());
		}
		
		return json_encode($reply);	
	}


	/**
	 * @brief set user send message into database
	 *
	 * @desc
	 *      when user send message, save message into database,
	 *      and update user active time
	 * 
	 * @param {String} send from user id
	 * @param {String} send to user id
	 * @param {String} message
	 * @param {UnixTime} time 
	 */
	public function setUserMessage($_send_from_id, $_send_to_id, $_message, $_time) 
	{
		if(empty($_send_from_id) || empty($_send_to_id) || empty($_message) || empty($_time))
		{
			throw new InvaildArgutmentException("Invaild send_from_id: ".var_export($_send_from_id,TRUE) . 
												" or send_to_id: ".var_export($_send_to_id, TRUE) .
												" or message: " .var_export($_message,TRUE) .
												" or time: ".var_export($_time,TRUE) );
		}

		$this->addMessage($_send_from_id, $_send_to_id, $_message, $_time);
		$this->setActiveTime($_send_from_id, $_time);
	
		return json_encode(array('time' => $_time));
	}

	/**
	 * @brief add message into database
	 *
	 * @desc
	 *      private function , add message into database
	 *
	 * @param {String} send from user id
	 * @param {String} send to user id
	 * @param {String} message
	 * @param {UnixTime} time
	 */
	private function addMessage($_from, $_to, $_text, $_time)
	{
		if(empty($_from) || empty($_to) || empty($_text) || empty($_time))
		{
			throw new InvaildArgutmentException("Invaild send_from_id: ".var_export($send_from_id,TRUE) . 
												" or send_to_id: ".var_export($send_to_id, TRUE) .
												" or message: " .var_export($message,TRUE) .
												" or time: ".var_export($time,TRUE) );
		}
		$result = FALSE;
		
		$exec = $this->connect->prepare('INSERT INTO '.$this->messageTable.' 
																(`send_from_id`,`send_to_id`,`time`,`message`) 
																VALUES 
																(:from, :to, :time,:message)');
		
		$exec->bindValue(':to', $_to);
		$exec->bindValue(':from', $_from);
		$exec->bindValue(':time', $_time);
		$exec->bindValue(':message', $_text);
		
		try{

			$exec->execute();
			$result = $exec->fetch(\PDO::FETCH_ASSOC);
					  $this->checkPDOerror($result, $exec);//throw RuntimeException
			$result = TRUE;
			
		} catch (Exception $_exception) {
			$this->logError($_exception->getMessage(), $_exception->getTraceAsString());
		}
		
		return $result;
	}

	/**
	 * @brief update activate 
	 *
	 * @desc
	 *
	 * @param {String} userID
	 * @param {UnixTime} time
	 */	
	private function setActiveTime($_id, $_time)
	{
		$result = FALSE;
		
		$exec = $this->connect->prepare('UPDATE '.$this->loginTable.' 
																SET `time`=:time 
																WHERE `id`= :id');
		$exec->bindValue(':id', $_id);
		$exec->bindValue(':time', $_time);
		
		try{

			$exec->execute();
			$result = $exec->fetch(\PDO::FETCH_ASSOC);
					  $this->checkPDOerror($result, $exec);//throw RuntimeException
			$result = TRUE;
		
		} catch (Exception $_exception) {
			$this->logError($_exception->getMessage(), $_exception->getTraceAsString());
		}
		
		return $result;
	}

	/**
	 * @brief check old messages from database
	 *
	 * @desc
	 *      check 10 old messages that send from Another to me
	 *		send From Another;
	 *		send To me
	 * 
	 * @param {String} send from id
	 * @param {String} send to id 
	 * @param {UnixTime} time
	 */
	public function checkOldMessages($_send_from_id, $_send_to_id, $_time) {
	/* get send from user name version */
		$exec = $this->connect->prepare('SELECT * FROM 
											( SELECT mess.send_from_id, mess.send_to_id, mess.time, mess.message, login.userName FROM `user_messages` AS mess
														LEFT JOIN user_login AS login 
														ON mess.send_from_id = login.id
												WHERE ((mess.send_to_id = :send_to_id AND mess.send_from_id = :send_from_id) 
													OR (mess.send_to_id = :send_from_id AND mess.send_from_id = :send_to_id)) 
													AND mess.time <= :time 
												ORDER BY mess.time DESC limit 10 ) 
											AS t1 ORDER BY `time` ASC '); //get last 10 messages
	
			
										
		$exec->bindValue(':send_to_id', $_send_to_id);
		$exec->bindValue(':send_from_id', $_send_from_id);
		$exec->bindValue(':time', $_time);
		
		$reply = array();
		
		try{

			$exec->execute();
			$result = $exec->fetchAll(\PDO::FETCH_ASSOC);
			//var_dump($result);
			//die();
			$reply = $this->checkPDOerror($result, $exec);//throw RuntimeException
			
		} catch (Exception $_exception) {
			$this->logError($_exception->getMessage(), $_exception->getTraceAsString());
		}
		
		return json_encode($reply);
	}

	/**
	 * @brief check New Messages
	 *
	 * @desc
	 *      check the latest message comming
	 *      response send from id (who send meesage to me),
	 *				 send to id (my id),
	 *               time,
	 *				 message,
	 *				 username
	 * 
	 * @param {String} user id
	 * @param {UnixtTime} latest message time
	 */
	public function checkNewMessages($_myId, $_latestMessageTime) 
	{
		if(empty($_myId) || empty($_latestMessageTime))
		{
			throw new InvaildArgutmentException("Invaild myId: ".var_export($_myId,TRUE) . " or lastMessageTime: " . var_export($_latestMessageTime,TRUE));
		}
	
		$exec = $this->connect->prepare('SELECT mess.send_from_id, mess.send_to_id, mess.time, mess.message, login.userName 
											FROM `user_messages` AS mess
												LEFT JOIN user_login AS login ON mess.send_from_id = login.id 
										WHERE `send_to_id` LIKE :myId 
										AND mess.time > :lastmTime 
										ORDER BY mess.time');

		$exec->bindValue(':myId', $_myId);
		$exec->bindValue(':lastmTime', $_latestMessageTime);

		$reply = array();
		
		try{

			$exec->execute();
			$result = $exec->fetchAll(\PDO::FETCH_ASSOC);
			$reply = $this->checkPDOerror($result, $exec);//throw RuntimeException
			
		} catch (Exception $_exception) {
			$this->logError($_exception->getMessage(), $_exception->getTraceAsString());
		}
		
		return json_encode($reply);
	}
	
	/**
	 * @brief user feed back set into database
	 *
	 * @desc
	 *      record user feed back into database
	 * @param {String} username
	 * @param {String} userID
	 * @param {String} feed back message
	 * @param {UnixTime} feedback time
	 * @param {String} userIP 
	 */
	public function usersFeedback($_userName,$_userId,$_feedbackContent,$_feedbackTime,$_userIp) 
	{
	
		$exec = $this->connect->prepare('INSERT INTO '.$this->feedbackTable.'
																(`user_name`,`user_id`,`feedback_content`,`feedback_time`,`ip`) 
																VALUES 
																(:userName,:userId,:feedbackContent,:feedbackTime,:userIp)');
	
		$exec->bindValue(':userName', $_userName);
		$exec->bindValue(':userId', $_userId);
		$exec->bindValue(':feedbackContent', $_feedbackContent);
		$exec->bindValue(':feedbackTime', $_feedbackTime);
		$exec->bindValue(':userIp', $_userIp);

		$exec->execute();
	}

	/**
	 * @brief PDO error
	 *
	 * @desc
	 *      
	 */	
	private function checkPDOerror($_result, $_exec)
	{
		$result = array();

			if(FALSE === $_result){
				$error_array = $_exec->errorInfo();
				$error = $error_array[2];
				if(is_string($error))// from some f**ked reason its comeing back FALSE instead of an empty array
				{throw new \RuntimeException($error);}
			}
			else
			{	$result = $_result;	}
			
		return $result;
	}
}
?>