<?php
 
class Handler
{
	private $value;
	private $db;
	
	public function __construct(array $_values)
	{
		$this->value = $_values;
		$this->db = new \Database();
	}

	public function process()
	{
		$resualt = '{}';
		
		if(isset($this->value['userID']) && isset($this->value['userName'])) 
		{
			$resualt = $this->setUserLogin($this->value['userID'],$this->value['userName']);
		} 
		elseif(isset($this->value['friendId']))
		{
			$resualt = $this->getContactExist($this->value['friendId']);
		}
		elseif(isset($this->value['lastActiveTime']) && isset($this->value['id']))
		{
			$resualt = $this->setNotification($this->value['id'], $this->value['lastActiveTime']);
		}
		elseif(isset($this->value['connectionId'])) 
		{  // check connections online or not online
			$resualt = $this->getOnlineContacts($this->value['connectionId']);
		}
		elseif(isset($this->value['new']) && isset($this->value['latestMessageTime']))
		{
			$resualt = $this->getNewMessages($this->value['new'],$this->value['latestMessageTime']);
		}
		elseif(isset($this->value['text']) && isset($this->value['sendFrom']) && isset($this->value['sendTo'])) 
		{ // textarea; send message to server 
			$message = htmlentities($this->value['text']);
			$resualt = $this->setNewMessage($this->value['sendFrom'], $this->value['sendTo'], $message);
		}
		elseif(isset($this->value['sendFrom']) && isset($this->value['sendTo']) && isset($this->value['time']))
		{  // check old messages
			$resualt = $this->getOldMessages($this->value['sendFrom'], $this->value['sendTo'], $this->value['time']);
		} 
		elseif(isset($this->value['feedbackContent']) && isset($this->value['userName']) && isset($this->value['userId']))
		{
			$feedbackContent = htmlentities($this->value['feedbackContent']);
			$resualt = $this->setUsersFeedback($this->value['userName'],$this->value['userId'],$feedbackContent);
		}
		else
		{
			throw new Exception("cannot process, value is : " . var_export($this->value, TRUE));
		}
		
		return $resualt;
	}
	
	private function setUserLogin($_user,$_userName){
	
		return $this->db->checkUser($_user,$_userName,time());
	}
	
	private function getContactExist($_friendId){
	
		return $this->db->checkFriendExist($_friendId);
	}

	private function setNotification($myId,$lastTime){
	
		return $this->db->checkNewMessages($myId,$lastTime);
	}
	
	private function getOnlineContacts($_connectionIds){
		
		return $this->db->checkUserStatus($_connectionIds);	
	}

	private function getNewMessages($_myId,$_latestTime)
	{
		return $this->db->checkNewMessages($_myId,$_latestTime);
	}
	
	private function setNewMessage($_fromID,$_toID,$_message){
	
		return $this->db->setUserMessage($_fromID,$_toID,$_message, microtime(true));
	}
	
	private function getOldMessages($_fromID,$_toID,$_time){
	
		return $this->db->checkOldMessages($_fromID,$_toID,$_time);
	}
	
	private function setUsersFeedback($_userName,$_userId,$_feedbackContent)	{
	
		return $this->db->usersFeedback($_userName,$_userId,$_feedbackContent,time(),$_SERVER['REMOTE_ADDR']);
	}
}
?>