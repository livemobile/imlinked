/**
 * bref: constructor function, for screen size controll
 */        
function Imlinked () {
    this.windowHeight = $(window).height();
    this.windowWidth = $(window).width();
}

/**
 * bref: check screen size <480
 *
 * @return: {Boolen}
 */
Imlinked.prototype.isSmallScreen = function () {
        if (this.windowHeight < 480 ||  this.windowWidth < 480)
            return true;
        else 
            return false;
};

/**
 * bref: check screen size >=480
 *
 * @return: {Boolen}
 */
Imlinked.prototype.isBigScreen = function () {
        if (this.windowHeight >= 480 &&  this.windowWidth >= 480) 
            return true;
        else 
            return false;
 };

/**
 * bref: build messageHtml
 * @param: {String} url of user img
 * @param: {String} message
 * @param: {String} userName
 * @param: {String} userID
 *
 * @return: {String} html string
 */
Imlinked.prototype.messageHtml = function (img, message, userName, id, time) {
    var html = '<article class="comment">\
                  <a class="comment-img profileLink" href="#" rel="'+id+'" target="_blank">\
                    <img src="'+img+'" alt="" width="50" height="50">\
                  </a>\
                  <div class="comment-body">\
                    <div class="text">\
                      <p class="secureTxt">'+message+'</p>\
                    </div>\
                    <p class="attribution">by <a class="profileLink" rel="'+id+'" href="#" target="_blank">'+userName+'</a> at '+time+'<!--time could be here--></p>\
                  </div>\
                </article>';
    return html;
};

/**
 * @brief parse date into readable formate
 *
 * @desc
 *      
 * @param {Date} date
 *
 * @return {String} time string like ---> h:m:s on m/d
 */
Imlinked.prototype.getMyTime = function (date) {
    
    function padzero(num){
        return num < 10 ? '0' + num : num;
    }

    var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'], 
        month = months[date.getMonth()],
        day = padzero(date.getDate()),
        hours = padzero(date.getHours()),
        minutes = padzero(date.getMinutes()),
        second = padzero(date.getSeconds()),

        timeString = hours +':'+ minutes +':'+ second +'  on  '+ month +' / '+ day;


  return timeString;
};


/**
 * @brief timezone convert
 *
 * @desc
 *      convert server time to local time and return formatted date string
 *
 * @param {UnixTime} serverTime
 *
 * @return {String} time string like ---> h:m:s on m/d
 */
Imlinked.prototype.timeConvert = function (serverTime) {

  var localTime = parseInt(serverTime) + (new Date().getTimezoneOffset() * 60),
      date = new Date(localTime*1000);

  return this.getMyTime(date);
};

/**
 * @brief
 *
 * @desc
 *      
 */
Imlinked.prototype.seach = function () {

}